DROP TABLE board_base;
DROP TABLE board_post;
DROP TABLE board_comment;
DROP TABLE attach_file;
DROP TABLE menu_action;


CREATE TABLE board_base (
	board_seq SERIAL,
	board_tp_cd VARCHAR(60),
	board_nm VARCHAR(255) NOT NULL,
	board_desc TEXT,
	row_status VARCHAR(20), create_date TIMESTAMP, create_by VARCHAR(255), modified_date TIMESTAMP, modified_by VARCHAR(255),
	CONSTRAINT idx_board_base_pk PRIMARY KEY (board_seq)
);


CREATE TABLE board_post (
	post_seq SERIAL,
	board_seq INTEGER NOT NULL,
	title VARCHAR(1000) NOT NULL,
	content TEXT NOT NULL,
	writer VARCHAR(60),
	tag VARCHAR(500),
	view_cnt BIGINT,
	row_status VARCHAR(20), create_date TIMESTAMP, create_by VARCHAR(255), modified_date TIMESTAMP, modified_by VARCHAR(255),
	CONSTRAINT idx_board_post_pk PRIMARY KEY (post_seq)
);


CREATE TABLE board_comment (
	comment_seq SERIAL,
	post_seq INTEGER NOT NULL,
	up_comment_seq INTEGER,
	comment TEXT NOT NULL,
	writer VARCHAR(60),
	row_status VARCHAR(20), create_date TIMESTAMP, create_by VARCHAR(255), modified_date TIMESTAMP, modified_by VARCHAR(255),
	CONSTRAINT idx_board_comment_pk PRIMARY KEY (comment_seq)
);


CREATE TABLE attach_file (
	file_seq SERIAL,
	orgn_tbl_nm VARCHAR(100),
	orgn_post_seq INTEGER,
	orgn_file_nm VARCHAR(200),
	saved_file_nm VARCHAR(200),
	file_size BIGINT,
	dn_cnt INTEGER,
	row_status VARCHAR(20), create_date TIMESTAMP, create_by VARCHAR(255), modified_date TIMESTAMP, modified_by VARCHAR(255),
	CONSTRAINT idx_attach_file_pk PRIMARY KEY (file_seq)
);


CREATE TABLE menu_action (
	menu_seq SERIAL,
	menu_nm VARCHAR(100),
	menu_url VARCHAR(255),
	menu_desc TEXT,
	sort_order INTEGER,
	row_status VARCHAR(20), create_date TIMESTAMP, create_by VARCHAR(255), modified_date TIMESTAMP, modified_by VARCHAR(255),
	CONSTRAINT idx_menu_action_pk PRIMARY KEY (menu_seq)
);


COMMENT ON TABLE board_base IS '게시판 기본';
COMMENT ON COLUMN board_base.board_seq IS '게시판 일련번호';
COMMENT ON COLUMN board_base.board_tp_cd IS '게시판 유형 코드';
COMMENT ON COLUMN board_base.board_nm IS '게시판 이름';
COMMENT ON COLUMN board_base.board_desc IS '게시판 설명';


COMMENT ON TABLE board_post IS '게시판 게시물';
COMMENT ON COLUMN board_post.post_seq IS '게시물 일련번호';
COMMENT ON COLUMN board_post.board_seq IS '게시판 일련번호';
COMMENT ON COLUMN board_post.title IS '제목';
COMMENT ON COLUMN board_post.content IS '내용';
COMMENT ON COLUMN board_post.writer IS '작성자';
COMMENT ON COLUMN board_post.tag IS '태그';
COMMENT ON COLUMN board_post.view_cnt IS '조회수';


COMMENT ON TABLE board_comment IS '게시판 댓글';
COMMENT ON COLUMN board_comment.comment_seq IS '댓글 일련번호';
COMMENT ON COLUMN board_comment.post_seq IS '게시물 일련번호';
COMMENT ON COLUMN board_comment.up_comment_seq IS '상위 댓글 일련번호';
COMMENT ON COLUMN board_comment.comment IS '내용';
COMMENT ON COLUMN board_comment.writer IS '작성자';


COMMENT ON TABLE attach_file IS '첨부파일';
COMMENT ON COLUMN attach_file.file_seq IS '파일 일련번호';
COMMENT ON COLUMN attach_file.orgn_tbl_nm IS '원본테이블명';
COMMENT ON COLUMN attach_file.orgn_post_seq IS '원본글 일련번호';
COMMENT ON COLUMN attach_file.orgn_file_nm IS '원본 파일명';
COMMENT ON COLUMN attach_file.saved_file_nm IS '저장 파일명';
COMMENT ON COLUMN attach_file.file_size IS '파일 크기';
COMMENT ON COLUMN attach_file.dn_cnt IS '다운로드 횟수';


COMMENT ON TABLE menu_action IS '메뉴실행';
COMMENT ON COLUMN menu_action.menu_seq IS '메뉴 일련번호';
COMMENT ON COLUMN menu_action.menu_nm IS '메뉴 명';
COMMENT ON COLUMN menu_action.menu_url IS '메뉴 URL';
COMMENT ON COLUMN menu_action.menu_desc IS '메뉴 설명';
COMMENT ON COLUMN menu_action.sort_order IS '정렬 순서';