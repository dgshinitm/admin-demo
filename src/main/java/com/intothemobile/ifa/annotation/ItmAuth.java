package com.intothemobile.ifa.annotation;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

@Retention(RUNTIME)
@Target({ TYPE, METHOD })
public @interface ItmAuth {
	public enum Role { SUPER, ADMIN, NORMAL }
	
	public Role[] role() default { Role.NORMAL };
}
