package com.intothemobile.ifa.sample.dao;

import com.intothemobile.ifa.ancestors.ItmMapper;
import com.intothemobile.ifa.sample.entity.User;

public interface UserMapper extends ItmMapper<User> {

}
