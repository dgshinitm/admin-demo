package com.intothemobile.mngr.cmmn.dao;

import org.springframework.stereotype.Repository;

import com.intothemobile.ifa.ancestors.ItmMapper;
import com.intothemobile.mngr.cmmn.entity.MenuAction;

@Repository("menuActionMapper")
public interface MenuActionMapper extends ItmMapper<MenuAction> {

}
