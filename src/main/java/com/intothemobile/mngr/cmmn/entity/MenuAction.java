package com.intothemobile.mngr.cmmn.entity;

import com.intothemobile.ifa.ancestors.ItmEntity;

public class MenuAction extends ItmEntity {

	private static final long serialVersionUID = -2728826607732229910L;
	
	private Integer menuSeq;
	private String menuNm;
	private String menuUrl;
	private String menuDesc;
	private Integer sortOrder;
	
	public Integer getMenuSeq() {
		return menuSeq;
	}
	public void setMenuSeq(Integer menuSeq) {
		this.menuSeq = menuSeq;
	}
	public String getMenuNm() {
		return menuNm;
	}
	public void setMenuNm(String menuNm) {
		this.menuNm = menuNm;
	}
	public String getMenuUrl() {
		return menuUrl;
	}
	public void setMenuUrl(String menuUrl) {
		this.menuUrl = menuUrl;
	}
	public String getMenuDesc() {
		return menuDesc;
	}
	public void setMenuDesc(String menuDesc) {
		this.menuDesc = menuDesc;
	}
	public Integer getSortOrder() {
		return sortOrder;
	}
	public void setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
	}

}
