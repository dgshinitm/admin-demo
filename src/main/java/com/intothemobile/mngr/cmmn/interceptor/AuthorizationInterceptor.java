package com.intothemobile.mngr.cmmn.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.intothemobile.ifa.annotation.ItmAuth;

public class AuthorizationInterceptor implements HandlerInterceptor {
	
	private static final Logger logger = LoggerFactory.getLogger(AuthorizationInterceptor.class);

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		// 1. handler 종류 확인
		// Controller에 있는 메서드인 경우만 적용하기 위해 HandlerMethod 타입인지 체크
		if( handler instanceof HandlerMethod == false ) {
			// return true이면  Controller에 있는 메서드가 아니므로, 그대로 컨트롤러로 진행
			return true;
		}

		// 2. 형 변환
		HandlerMethod handlerMethod = (HandlerMethod) handler;
		
		// 3. @Auth 받아오기
		ItmAuth auth = handlerMethod.getMethodAnnotation(ItmAuth.class);
		ItmAuth adminRole = handlerMethod.getMethod().getDeclaringClass().getAnnotation(ItmAuth.class);
		
		if (logger.isDebugEnabled()) { logger.debug("auth is --> " + auth); }
		if (logger.isDebugEnabled()) { logger.debug("adminRole is --> " + adminRole); }
		
		// 4. method에 @Auth가 없는 경우, 즉 인증이 필요 없는 요청
		if( auth == null && adminRole == null ) {
			return true;
		}
		
		// 5. @ItmAuth가 있는 경우이므로, 세션이 있는지 체크
		HttpSession session = request.getSession();
		if( session == null ) {
			// 로그인 화면으로 이동
			response.sendRedirect(request.getContextPath() + "/login/form");
			return false;
		}
		
		// 6. 세션이 존재하면 유효한 유저인지 확인
//		PrtlSvcUsr authUser = (PrtlSvcUsr) session.getAttribute("loginUser");
//		if ( authUser == null ) {
//			response.sendRedirect(request.getContextPath() + "/login/form");
//			return false;
//		}

		// 7. admin일 경우
		if (adminRole != null) {
			ItmAuth.Role[] role = adminRole.role();
			
			if(logger.isDebugEnabled()) { logger.debug("Annotation role : " + role); }
//			if(logger.isDebugEnabled()) { logger.debug("User role : " + authUser.getUsrType()); }
			
			String[] adminTypes = {};
			
			for (ItmAuth.Role r: role) {
				adminTypes = (String[]) ArrayUtils.add(adminTypes, StringUtils.lowerCase(r.toString()));
				logger.debug(r.toString());
			}
			
			if (logger.isDebugEnabled()) {
				for (String s: adminTypes) {
					logger.debug(s);
				}
			}
			
//			if (!ArrayUtils.contains(adminTypes, authUser.getUsrType())) {
//				response.sendRedirect(request.getContextPath() + "/login/form");
//				return false;
//			}
		}
		
		// 8. 접근허가, 즉 메서드를 실행하도록 함
		return true;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		// do nothing...
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		// do nothing...
	}

}
