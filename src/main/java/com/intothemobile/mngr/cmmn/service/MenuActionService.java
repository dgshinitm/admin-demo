package com.intothemobile.mngr.cmmn.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.intothemobile.mngr.cmmn.dao.MenuActionMapper;
import com.intothemobile.mngr.cmmn.entity.MenuAction;
import com.intothemobile.ifa.ancestors.ItmSimpleService;

@Service
public class MenuActionService implements ItmSimpleService<MenuAction> {
	
	@Autowired
	private MenuActionMapper menuActionMapper;

	@Override
	public int add(MenuAction entity) throws Exception {
		return menuActionMapper.insert(entity);
	}

	@Override
	public int edit(MenuAction entity) throws Exception {
		return menuActionMapper.update(entity);
	}

	@Override
	public int remove(MenuAction entity) throws Exception {
		return menuActionMapper.delete(entity);
	}

	@Override
	public Long getTotal(MenuAction entity) throws Exception {
		return menuActionMapper.selectTotal(entity);
	}

	@Override
	public MenuAction find(MenuAction entity) throws Exception {
		return menuActionMapper.select(entity);
	}

	@Override
	public List<MenuAction> findList(MenuAction entity) throws Exception {
		return menuActionMapper.selectList(entity);
	}

}
