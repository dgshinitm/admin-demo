package com.intothemobile.mngr.cmmn.web;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.intothemobile.ifa.ancestors.ItmSimpleController;
import com.intothemobile.mngr.cmmn.entity.MenuAction;
import com.intothemobile.mngr.cmmn.service.MenuActionService;

@RestController
@RequestMapping("/api/menu")
public class MenuActionApiController extends ItmSimpleController {
	
	private final Logger logger = LoggerFactory.getLogger(MenuActionApiController.class);
	
	@Autowired
	private MenuActionService menuActionService;

	@RequestMapping("/list")
	public Map<String, Object> list() throws Exception {
//		List<String[]> list = new ArrayList<String[]>();
//		
//		list.add(new String[] {"1", "메뉴실행관리", "/frame/menu-action/list", "메뉴 실행단위 주소를 관리합니다.", "1"});
//		list.add(new String[] {"2", "메뉴실행관리", "/frame/menu-action/list", "메뉴 실행단위 주소를 관리합니다.", "2"});
//		list.add(new String[] {"3", "메뉴실행관리", "/frame/menu-action/list", "메뉴 실행단위 주소를 관리합니다.", "3"});
//		list.add(new String[] {"4", "메뉴실행관리", "/frame/menu-action/list", "메뉴 실행단위 주소를 관리합니다.", "4"});
		
		List<MenuAction> list = menuActionService.findList(null);
		Long recordTotal = menuActionService.getTotal(null);
		Long recordsFiltered = menuActionService.getTotal(null);
		
		Map<String, Object> result = new HashMap<String, Object>();
		
		Long now = (new Date()).getTime();
		
		result.put("draw", now);
		result.put("recordsTotal", recordTotal);
		result.put("recordsFiltered", recordsFiltered);
		result.put("data", list);
		
		if (logger.isDebugEnabled()) { logger.debug("result = " + result); }
		if (logger.isDebugEnabled()) { logger.debug("list = " + list); }
		
		return result;
	}

}
