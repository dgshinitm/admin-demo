package com.intothemobile.mngr.cmmn.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.intothemobile.ifa.ancestors.ItmSimpleController;

@Controller
@RequestMapping("/frame/menu")
public class MenuActionController extends ItmSimpleController {

	private final Logger logger = LoggerFactory.getLogger(MenuActionController.class);
	private final static String REQ_PREFIX = "frame/menuAction";
	private final static String REQ_SUFFIX = ".layout";
	
	@Value("#{configProperties['admin.page.template']}")
	private String ADMIN_PAGE_TEMPLATE;
	
	@RequestMapping("/list")
	public ModelAndView list() throws Exception {
		String viewName = ADMIN_PAGE_TEMPLATE + "/" + REQ_PREFIX + "/list" + REQ_SUFFIX;
		ModelAndView mView = new ModelAndView(viewName);
		
		if (logger.isDebugEnabled()) { logger.debug("MenuActionController.list - " + viewName); }
		
		return mView;
	}

}
