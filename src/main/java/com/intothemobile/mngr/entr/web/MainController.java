package com.intothemobile.mngr.entr.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.intothemobile.ifa.ancestors.ItmSimpleController;

@Controller
@RequestMapping("/main")
public class MainController extends ItmSimpleController {
	private final Logger logger = LoggerFactory.getLogger(MainController.class);
	private final static String REQ_PREFIX = "main";
	private final static String REQ_SUFFIX = ".layout";
	
	@Value("#{configProperties['admin.page.template']}")
	private String ADMIN_PAGE_TEMPLATE;
	
	@RequestMapping("/index")
	public ModelAndView index() throws Exception {
		String viewName = ADMIN_PAGE_TEMPLATE + "/" + REQ_PREFIX + "/index" + REQ_SUFFIX;
		ModelAndView mView = new ModelAndView(viewName);
		
		if (logger.isDebugEnabled()) { logger.debug("MainController.index - " + viewName); }
		
		return mView;
	}

}
