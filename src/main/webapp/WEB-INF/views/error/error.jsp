<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<c:set var="baseUrl" value="${pageContext.request.contextPath}" />

<html>
<head>
	<link href="${baseUrl}/resources/css/ifa.css" rel="stylesheet" type="text/css" />
	<script src="http://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
</head>
<body>
<h1>Error</h1>

<dl>
	<dt>errorCode</dt>
	<dd>${errorCode}</dd>
	<dt>errorMessage</dt>
	<dd>${errorMessage}</dd>
	<dt>isDebugMode</dt>
	<dd>${isDebugMode}</dd>
	<dt>Result</dt>
	<dd>${Result}</dd>
	<dt>Message</dt>
	<dd>${Message}</dd>
</dl>
</body>
</html>
