<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<c:set var="baseUrl" value="${pageContext.request.contextPath}" />

<div class="row">
	<div class="box col-md-12">
		<div class="box-inner">
			<div class="box-header well" data-original-title="">
				<h2><i class="glyphicon glyphicon-star-empty"></i> Menu Action List</h2>
	
				<div class="box-icon">
<!-- 					<a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a> -->
<!-- 					<a href="#" class="btn btn-minimize btn-round btn-default"><i class="glyphicon glyphicon-chevron-up"></i></a> -->
<!-- 					<a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a> -->
				</div>
			</div>
			<div class="box-content">
				<table id="data-table-main" class="table table-striped table-bordered responsive" style="width: 100%;">
<!-- 				table table-striped table-bordered responsive -->
					<thead>
						<tr>
							<th>No.</th>
							<th>메뉴명</th>
							<th>메뉴URL</th>
							<th>메뉴설명</th>
							<th>정렬순서</th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<th>No.</th>
							<th>메뉴명</th>
							<th>메뉴URL</th>
							<th>메뉴설명</th>
							<th>정렬순서</th>
						</tr>
					</tfoot>
				</table>
			</div>
		</div>
	</div>
</div><!--/row-->

<script type="text/javascript">
$(document).ready(function() {
	$("#data-table-main").DataTable({
		"ajax": "${baseUrl}/api/menu/list"
		,"columns": [
			{ "data": "menu_seq" },
			{ "data": "menu_nm" },
			{ "data": "menu_url" },
			{ "data": "menu_desc" },
			{ "data": "sort_order" }
		]
		,"serverSide": true
		,"dom": "<'row'<'col-md-6'l><'col-md-6'f>r>t<'row'<'col-md-12'i><'col-md-12 center-block'p>>"
		,"pagingType": "bootstrap"
		,"language": {
			"info" : "_START_ - _END_ [ _TOTAL_ ]", // _START_ _END_ _TOTAL_ _MAX_ _PAGE_ _PAGES_
			"lengthMenu": "_MENU_ per page"
		}
	});
});
</script>

<style>
.dataTables_filter , .dataTables_info { float: right; }
</style>