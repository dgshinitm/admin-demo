<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<c:set var="baseUrl" value="${pageContext.request.contextPath}" />
<c:set var="charismaUrl" value="${pageContext.request.contextPath}/resources/template/charisma" />

<!-- The styles -->
<link id="bs-css" href="${charismaUrl}/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

<link href="${charismaUrl}/css/charisma-app.css" rel="stylesheet">
<link href='${charismaUrl}/bower_components/fullcalendar/dist/fullcalendar.css' rel='stylesheet'>
<link href='${charismaUrl}/bower_components/fullcalendar/dist/fullcalendar.print.css' rel='stylesheet' media='print'>
<link href='${charismaUrl}/bower_components/chosen/chosen.min.css' rel='stylesheet'>
<link href='${charismaUrl}/bower_components/colorbox/example3/colorbox.css' rel='stylesheet'>
<link href='${charismaUrl}/bower_components/responsive-tables/responsive-tables.css' rel='stylesheet'>
<link href='${charismaUrl}/bower_components/bootstrap-tour/build/css/bootstrap-tour.min.css' rel='stylesheet'>
<link href='${charismaUrl}/css/jquery.noty.css' rel='stylesheet'>
<link href='${charismaUrl}/css/noty_theme_default.css' rel='stylesheet'>
<link href='${charismaUrl}/css/elfinder.min.css' rel='stylesheet'>
<link href='${charismaUrl}/css/elfinder.theme.css' rel='stylesheet'>
<link href='${charismaUrl}/css/jquery.iphone.toggle.css' rel='stylesheet'>
<link href='${charismaUrl}/css/uploadify.css' rel='stylesheet'>
<link href='${charismaUrl}/css/animate.min.css' rel='stylesheet'>

<!-- <link href='//cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css' rel='stylesheet'> -->


<!-- The fav icon -->
<link rel="shortcut icon" href="${baseUrl}/resources/images/itm_main_logo.png">

<link href="${baseUrl}/resources/css/ifa.css" rel="stylesheet" type="text/css" />