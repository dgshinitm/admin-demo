<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<c:set var="baseUrl" value="${pageContext.request.contextPath}" />
<c:set var="charismaUrl" value="${pageContext.request.contextPath}/resources/template/charisma" />

<script type="text/javascript">
var IFA_GP = {
	"BASE_URL" : "${baseUrl}",
	"CHARISMA_URL" : "${charismaUrl}"
};
</script>

<!-- jQuery -->
<script src="${charismaUrl}/bower_components/jquery/jquery.js"></script>
<%-- <script src="${charismaUrl}/bower_components/jquery/jquery.min.js"></script> --%>

<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<script src="${baseUrl}/resources/js/ifa-common.js"></script>
