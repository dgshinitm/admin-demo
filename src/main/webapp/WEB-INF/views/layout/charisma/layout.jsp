<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<c:set var="baseUrl" value="${pageContext.request.contextPath}" />
<c:set var="charismaUrl" value="${pageContext.request.contextPath}/resources/template/charisma" />

<!DOCTYPE html>
<html lang="ko">
<head>
<tiles:insertAttribute name="header-meta"></tiles:insertAttribute>
<title>Sample site for itm-framework-assistant.</title>
<tiles:insertAttribute name="header-css"></tiles:insertAttribute>
<tiles:insertAttribute name="header-js"></tiles:insertAttribute>
</head>
<body>
<!-- head -->
<tiles:insertAttribute name="page-header"></tiles:insertAttribute>

<div class="ch-container">
	<div class="row">
	
		<tiles:insertAttribute name="page-left"></tiles:insertAttribute>
		
		<noscript>
			<div class="alert alert-block col-md-12">
				<h4 class="alert-heading">Warning!</h4>
				<p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
			</div>
		</noscript>
		
		<div id="content" class="col-lg-10 col-sm-10">
			<div>
				<ul class="breadcrumb">
					<li><a href="#">Home</a></li>
					<li><a href="#">Blank</a></li>
				</ul>
			</div>
			<tiles:insertAttribute name="page-main"></tiles:insertAttribute>
		</div>
	
	
	</div>
</div>

	<hr />
	
	<!-- footer -->
	<tiles:insertAttribute name="page-footer"></tiles:insertAttribute>

<!-- external javascript -->

<script src="${charismaUrl}/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- library for cookie management -->
<script src="${charismaUrl}/js/jquery.cookie.js"></script>
<!-- calender plugin -->
<script src='${charismaUrl}/bower_components/moment/min/moment.min.js'></script>
<script src='${charismaUrl}/bower_components/fullcalendar/dist/fullcalendar.min.js'></script>
<!-- data table plugin -->
<%-- <script src='${charismaUrl}/js/jquery.dataTables.min.js'></script> --%>
<script type="text/javascript" src="//cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>

<!-- select or dropdown enhancer -->
<script src="${charismaUrl}/bower_components/chosen/chosen.jquery.min.js"></script>
<!-- plugin for gallery image view -->
<script src="${charismaUrl}/bower_components/colorbox/jquery.colorbox-min.js"></script>
<!-- notification plugin -->
<script src="${charismaUrl}/js/jquery.noty.js"></script>
<!-- library for making tables responsive -->
<script src="${charismaUrl}/bower_components/responsive-tables/responsive-tables.js"></script>
<!-- tour plugin -->
<script src="${charismaUrl}/bower_components/bootstrap-tour/build/js/bootstrap-tour.min.js"></script>
<!-- star rating plugin -->
<script src="${charismaUrl}/js/jquery.raty.min.js"></script>
<!-- for iOS style toggle switch -->
<script src="${charismaUrl}/js/jquery.iphone.toggle.js"></script>
<!-- autogrowing textarea plugin -->
<script src="${charismaUrl}/js/jquery.autogrow-textarea.js"></script>
<!-- multiple file upload plugin -->
<script src="${charismaUrl}/js/jquery.uploadify-3.1.min.js"></script>
<!-- history.js for cross-browser state change on ajax -->
<script src="${charismaUrl}/js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<script src="${charismaUrl}/js/charisma.js"></script>

</body>
</html>