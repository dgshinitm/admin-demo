<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="baseUrl" value="${pageContext.request.contextPath}" />

<footer class="row">
	<p class="col-md-9 col-sm-9 col-xs-12 copyright">&copy;
		<a href="http://www.intothemobile.com" target="_blank">Into The Mobile, inc.</a>
		2012 - 2020
	</p>
	<p class="col-md-3 col-sm-3 col-xs-12 powered-by">
		Powered by: <a href="https://github.com/dgshinitm/itm-framework-assistant">ITM Framework Assistant</a>
	</p>
</footer>
